package com.haladyj.SS2.service;

import com.haladyj.SS2.model.User;
import com.haladyj.SS2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public class JPAUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> optUser = userRepository.findUserByUsername(username);

        User user = optUser.orElseThrow(()->new UsernameNotFoundException("Username does not exist in database scope"));

        return new SecurityUser(user);
    }
}
